# shelf_auth_session

[![Build Status](https://drone.io/bitbucket.org/andersmholmgren/shelf_auth_session/status.png)](https://drone.io/bitbucket.org/andersmholmgren/shelf_auth_session/latest)
[![Pub Version](http://img.shields.io/pub/v/shelf_auth_session.svg)](https://pub.dartlang.org/packages/shelf_auth_session)

Session support for shelf_auth

TODO: ideally this would be merged or at least share code with shelf_simple_session

## Usage

Coming ...

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/andersmholmgren/shelf_auth_session/issues
