# Changelog

## 0.4.11

- updated crypto dependency with required code changes

## 0.4.10

* upgrade shelf_auth dependency

## 0.4.0

* upgrade option, shelf and shelf_auth versions

## 0.3.0

* require shelf_auth 0.5.0

## 0.2.0

* upgrade shelf_auth dependency

## 0.0.1

* Initial version, created by Stagehand.
