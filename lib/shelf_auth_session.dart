// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

/// The shelf_auth_session library.
///
/// This is an awesome library. More dartdocs go here.
library shelf_auth_session;

export 'src/inmem_session_repository.dart';
export 'src/session.dart';
export 'src/shelf_auth_session_base.dart';
