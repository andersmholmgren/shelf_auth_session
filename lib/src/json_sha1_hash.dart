library json.sha1.hash;

import 'dart:convert' hide Base64Codec;

import 'package:crypto/crypto.dart' show Base64Codec;

// SHA-1 hash of normalised model json
// TODO: Normalise model
String createJsonHashTag(dynamic model) => createHash(JSON.encode(model));

String createHash(String str) {
  final hashBytes = sha1.convert(str.codeUnits);
  final hash = const Base64Codec(urlSafe: true).encode(hashBytes);
  //  print('hash is $hash');
  return hash;
}
