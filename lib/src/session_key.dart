// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library shelf_auth_session.session.key;

import 'json_sha1_hash.dart';

// assuming usernames are unique then this should be safe
String sessionKey(String username, DateTime sessionCreatedTime) =>
    createJsonHashTag({
      'username': username,
      'sessionCreatedTime': sessionCreatedTime.millisecondsSinceEpoch
    });
