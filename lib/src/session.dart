// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library shelf_auth_session.session;

import 'package:collection/collection.dart';

class Session extends DelegatingMap {
  final String sessionKey;

  bool _isDirty = false;
  bool get isDirty => _isDirty;

  Session(this.sessionKey) : super({});

  void markDirty() {
    _isDirty = true;
  }

  @override
  operator []=(key, value) {
    markDirty();
    super[key] = value;
  }

  @override
  void clear() {
    markDirty();
    super.clear();
  }

  @override
  remove(Object key) {
    markDirty();
    return super.remove(key);
  }

  @override
  void addAll(Map other) {
    markDirty();
    super.addAll(other);
  }

  @override
  putIfAbsent(key, ifAbsent()) {
    markDirty(); // TODO: not necessarily dirty
    return super.putIfAbsent(key, ifAbsent);
  }
}
