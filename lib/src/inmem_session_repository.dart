// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library shelf_auth_session.repository.inmem;

//import 'package:shelf_simple_session/shelf_simple_session.dart';
import 'dart:async';

import 'package:shelf_auth/shelf_auth.dart';
import 'package:shelf_auth_session/src/session.dart';

import 'shelf_auth_session_base.dart';

class InMemorySessionRepository implements SessionRepository {
  final Map<String, Session> _sessionMap = {};

  @override
  Future<Session> getOrCreate(
      String sessionKey, SessionAuthenticatedContext context) {
    var session = _sessionMap[sessionKey];
    if (session == null) {
      session = new Session(sessionKey);
      _sessionMap[sessionKey] = session;
    }

    return new Future.value(session);
  }

  @override
  Future save(Session session) {
    _sessionMap[session.sessionKey] = session;
    return new Future.value();
  }
}
