// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library shelf_auth_session.base;

//import 'package:shelf_simple_session/shelf_simple_session.dart';
import 'dart:async';

import 'package:option/option.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_auth/shelf_auth.dart';

import 'session.dart';
import 'shelf_auth_session_impl.dart';

const String CTX_SESSION_KEY = 'shelf_auth_session.session';

Option<Session> session(Request req) =>
    new Option<Session>(req.context[CTX_SESSION_KEY]);

abstract class SessionRepository {
  Future<Session> getOrCreate(
      String sessionKey, SessionAuthenticatedContext context);

  Future save(Session session);

  // TODO: need to find a way to hook this in. e.g. if the user logs out
//  Future destroy(String sessionKey);
}

abstract class SessionStore {
  factory SessionStore(SessionRepository repository) {
    return new ShelfAuthSessionStoreImpl(repository);
  }

  Future<Request> loadSession(Request request);

  Future<Response> storeSession(Request request, Response response);
}

Middleware sessionMiddleware(SessionRepository sessionRepository) {
  final sessionStore = new SessionStore(sessionRepository);

  return (Handler innerHandler) {
    return (Request request) {
      return sessionStore.loadSession(request).then((r) {
        return new Future.sync(() => innerHandler(r)).then((Response response) {
          return sessionStore.storeSession(r, response);
        });
      });
    };
  };
}
