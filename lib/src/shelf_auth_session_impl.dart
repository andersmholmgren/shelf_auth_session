// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library shelf_auth_session.impl;

//import 'package:shelf_simple_session/shelf_simple_session.dart';
import 'dart:async';

import 'package:option/option.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_auth/shelf_auth.dart';

import 'session_key.dart';
import 'shelf_auth_session_base.dart';

class ShelfAuthSessionStoreImpl implements SessionStore {
  final SessionRepository _repository;

  ShelfAuthSessionStoreImpl(this._repository) {
//    ensure(_repository, isNotNull);
  }

  @override
  Future<Request> loadSession(Request request) {
    return authenticatedSessionContext().map((sessionContext) async {
      final session = await _repository.getOrCreate(
          sessionKey(sessionContext.principal.name,
              sessionContext.sessionFirstCreated),
          sessionContext);

      return request.change(context: {CTX_SESSION_KEY: session});
    }).getOrElse(() => new Future.value(request));
  }

  @override
  Future<Response> storeSession(Request request, Response response) {
    return session(request)
        .expand((s) => s.isDirty ? new Some(s) : const None())
        .map((dirtySession) =>
            _repository.save(dirtySession).then((_) => response))
        .getOrElse(() => new Future.value(response));
  }
}
